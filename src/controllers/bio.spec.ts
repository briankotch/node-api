import type { IJob } from "../models/bio";
import bioController from "./bio";
import dotenv from "dotenv";
dotenv.config();

const testEmail: string = process.env.CONTACT_EMAIL as string;
const testNumber: string = process.env.CONTACT_NUMBER as string;

it("should return a profile", () => {
    expect(bioController.bio.first_name).toEqual("Brian");
    expect(bioController.bio.last_name).toEqual("Hatchet (Kotch)");
    expect(bioController.bio.contact_email).toEqual(testEmail);
    expect(bioController.bio.contact_number).toEqual(testNumber);
    expect(bioController.jobs).toBeInstanceOf(Array);
});

// Why yes, you can dynamically introspect json tyes
it("should filter jobs by tags", () => {
    let jobs: IJob[] = bioController.filterJobs(["aws"]);

    expect(jobs.length).toBeGreaterThan(0);

    jobs.forEach((job: IJob) => {
        expect(job.tags).toContain("aws");
    });

    jobs = bioController.filterJobs(["nonexistent tag"]);

    expect(jobs.length).toEqual(0);
});
