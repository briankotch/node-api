import * as express from "express";
import { Request, Response, Router } from "express";
import bioController from "../controllers/bio";

const BioRouter: Router = express.Router();

BioRouter.get("/v1/bio", (req: Request, res: Response) => {
    return res.send(bioController.bio);
});

export default BioRouter;
