import { EmailSender, MailOptions } from "./email";

it("should should contruct an email client", () => {
    const email: EmailSender = new EmailSender();
    const mail = new MailOptions(
        "test@test.com",
        "receive@receive.com",
        "test subject",
        "test body"
    );

    expect(email).toBeInstanceOf(EmailSender);

    const response = email.sendMail(mail);
});
